import firebase from 'firebase/app';
import 'firebase/database';

const config = {
  apiKey: 'AIzaSyCpigEBV57-8OWKGTovUCOOBW3BUDZCfu4',
  authDomain: 'superfootball-55511.firebaseapp.com',
  databaseURL: 'https://superfootball-55511.firebaseio.com',
  projectId: 'superfootball-55511',
  storageBucket: 'superfootball-55511.appspot.com',
  messagingSenderId: '273997459249',
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();

export default db;
