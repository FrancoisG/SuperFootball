import firebase from 'firebase';

const createUserAction = creds => ({
  type: 'CREATE_USER_REQUEST',
  payload: creds,
});

const createUserSuccessAction = user => ({
  type: 'CREATE_USER_SUCCESS',
  payload: user,
});

const createUserErrorAction = error => ({
  type: 'CREATE_USER_ERROR',
  payload: error,
});

const loginUserAction = creds => ({
  type: 'LOGIN_USER_REQUEST',
  payload: creds,
});

const loginUserSuccessAction = user => ({
  type: 'LOGIN_USER_SUCCESS',
  payload: user,
});

const loginUserErrorAction = error => ({
  type: 'LOGIN_USER_ERROR',
  payload: error,
});

const disconnectUserAction = () => ({
  type: 'DISCONNECT_USER_REQUEST',
});

const disconnectUserSuccessAction = () => ({
  type: 'DISCONNECT_USER_SUCCESS',
});

const disconnectUserErrorAction = () => ({
  type: 'DISCONNECT_USER_FAILURE',
});

const userIsSignedIn = user => ({
  type: 'LOGIN_USER_SUCCESS',
  payload: user,
});

const userIsNotSignedIn = () => ({
  type: 'LOGIN_USER_FAILURE',
  payload: '',
});

const updateUserRequest = () => ({
  type: 'UPDATE_USER_REQUEST',
});

const updateUserSuccess = user => ({
  type: 'UPDATE_USER_SUCCESS',
  payload: user,
});

const updateUserFailure = error => ({
  type: 'UPDATE_USER_FAILURE',
  payload: error,
});

export const loginUserService = creds => (dispatch) => {
  if (creds.username === '' || creds.password === '') {
    dispatch(loginUserErrorAction('Please enter email and password.'));
  } else if (creds.username === '') {
    dispatch(loginUserErrorAction('Email is invalid.'));
  } else {
    dispatch(loginUserAction(creds));
    firebase.auth()
      .signInWithEmailAndPassword(creds.username, creds.password)
      .then((authUser) => {
        dispatch(loginUserSuccessAction(authUser.user));
      })
      .catch((error) => {
        dispatch(loginUserErrorAction(error.message));
      });
  }
};

export const createUserService = creds => (dispatch) => {
  if (creds.username === '' || creds.password === ''
        || creds.passwordConfirmation === '') {
    dispatch(createUserErrorAction('Please enter email and password.'));
  } else if (creds.username === '') {
    dispatch(createUserErrorAction('Email is invalid.'));
  } else if (creds.passwordConfirmation !== creds.password) {
    dispatch(createUserErrorAction('Password are differents.'));
  } else {
    dispatch(createUserAction(creds));
    firebase.auth()
      .createUserWithEmailAndPassword(creds.username, creds.password)
      .then((authUser) => {
        dispatch(createUserSuccessAction(authUser.user));
      })
      .catch((error) => {
        dispatch(createUserErrorAction(error.message));
      });
  }
};

export const disconnectUserService = () => (dispatch) => {
  dispatch(disconnectUserAction());
  firebase.auth().signOut().then(() => {
    dispatch(disconnectUserSuccessAction());
  }).catch(() => {
    dispatch(disconnectUserErrorAction());
  });
};

export const isUserConnectedService = () => (dispatch) => {
  const user = firebase.auth().currentUser;
  if (user) {
    dispatch(userIsSignedIn(user));
    return true;
  }
  dispatch(userIsNotSignedIn());
  return false;
};

export const updateUserService = object => (dispatch) => {
  const user = firebase.auth().currentUser;
  dispatch(updateUserRequest());
  user.updateProfile(object).then(() => {
    dispatch(updateUserSuccess(firebase.auth().currentUser));
  }).catch((error) => {
    dispatch(updateUserFailure(error));
  });
};
