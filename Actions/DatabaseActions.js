import db from '../firebase/firebase';

const teamsRequestAction = () => ({
  type: 'TEAMS_REQUEST',
});

const teamsUpdateAction = () => ({
  type: 'TEAMS_UPDATE',
});

const teamsSuccessAction = teams => ({
  type: 'TEAMS_SUCCESS',
  payload: teams,
});

const profilePictureRequestAction = () => ({
  type: 'PICTURE_REQUEST',
});

const profilePictureUpdateAction = () => ({
  type: 'PICTURE_UPDATE',
});

const profilePictureSuccessAction = picture => ({
  type: 'PICTURE_SUCCESS',
  payload: picture,
});

export const getSubscribedTeamsService = user => (dispatch) => {
  dispatch(teamsRequestAction());
  return db.ref(`${user.uid}/teams`).once('value').then((results) => {
    if (results.val() === null) {
      return dispatch(teamsSuccessAction([]));
    }
    return dispatch(teamsSuccessAction(results.val()));
  });
};

export const updateSubscribedTeamsService = (user, teams) => (dispatch) => {
  dispatch(teamsUpdateAction());
  db.ref(`${user.uid}/teams`).set(teams);
  return dispatch(getSubscribedTeamsService(user));
};

export const getProfilePictureService = user => (dispatch) => {
  dispatch(profilePictureRequestAction());
  return db.ref(`${user.uid}/profilePicture`).once('value').then((results) => {
    if (results.val() === null) {
      return dispatch(profilePictureSuccessAction(''));
    }
    return dispatch(profilePictureSuccessAction(results.val()));
  });
};

export const updateProfilePictureService = (user, picture) => (dispatch) => {
  dispatch(profilePictureUpdateAction());
  db.ref(`${user.uid}/profilePicture`).set(picture);
  return dispatch(getProfilePictureService(user));
};
