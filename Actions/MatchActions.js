import axios from 'axios';
import moment from 'moment';

moment.locale('fr');

const requestMatchs = () => ({
  type: 'MATCH_REQUEST',
});

const receiveMatchs = json => ({
  type: 'MATCH_SUCCESS',
  matchs: json,
});

const errorMatchs = error => ({
  type: 'MATCH_FAILURE',
  error,
});

const formatMatch = result => result.matches.map(match => ({
  awayTeam: match.awayTeam.id,
  homeTeam: match.homeTeam.id,
  competition: match.competition.id,
  scoreAway: match.score.fullTime.awayTeam,
  scoreHome: match.score.fullTime.homeTeam,
  date: match.utcDate,
  status: match.status,
  winner: match.score.winner,
  id: match.id,
}));

export default function getMatchService(teamsArray) {
  return (dispatch) => {
    dispatch(requestMatchs());
    const matchArray = {};
    const dateFrom = moment().subtract(1, 'months').format('YYYY-MM-DD');
    const dateTo = moment().add(1, 'months').format('YYYY-MM-DD');
    const promiseArray = teamsArray.map(team => axios.get(`http://api.football-data.org/v2/teams/${team.id}/matches?dateFrom=${dateFrom}&dateTo=${dateTo}&competitions=2015`, {
      headers: {
        authorization: 'Bearer',
        'X-Auth-Token': 'aaa9662cc0514cc8aa065f3653583178',
      },
    }).then((response) => {
      matchArray[team.id] = formatMatch(response.data);
      return response;
    }).catch(error => error));

    return Promise.all(promiseArray).then(() => {
      dispatch(receiveMatchs(matchArray));
      return matchArray;
    }).catch((error) => {
      dispatch(errorMatchs(error));
    });
  };
}
