import logo511 from './511.png';
import logo514 from './514.png';
import logo516 from './516.png';
import logo518 from './518.png';
import logo521 from './521.png';
import logo522 from './522.png';
import logo523 from './523.png';
import logo524 from './524.png';
import logo526 from './526.png';
import logo527 from './527.png';
import logo528 from './528.png';
import logo529 from './529.png';
import logo530 from './530.png';
import logo532 from './532.png';
import logo538 from './538.png';
import logo543 from './543.png';
import logo547 from './547.png';
import logo548 from './548.png';
import logo556 from './556.png';
import logo576 from './576.png';

const logo = {
  511: logo511,
  514: logo514,
  516: logo516,
  518: logo518,
  521: logo521,
  522: logo522,
  523: logo523,
  524: logo524,
  526: logo526,
  527: logo527,
  528: logo528,
  529: logo529,
  530: logo530,
  532: logo532,
  538: logo538,
  543: logo543,
  547: logo547,
  548: logo548,
  556: logo556,
  576: logo576,
};

export default logo;
