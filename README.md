# SuperFootball
This is a ReactNative application made by Francois, Nicolas and Cyril

# Installation
```
npm install
npm start
```

# Recap followup
-[ ] Update Readme (en last)
-[x] React.purecomponent et stateless components
-[ ] Saga Middleware
-[ ] Test unitaire jest
-[ ] Animations
-[ ] Sortir tout les inlines styles
