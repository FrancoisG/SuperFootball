const matchs = (state = {}, action) => {
  switch (action.type) {
    case 'MATCH_SUCCESS':
      return {
        ...state,
        match: action.matchs,
      };
    default:
      return state;
  }
};
export default matchs;
