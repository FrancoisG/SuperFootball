import { combineReducers } from 'redux';
import authentification from './authentification';
import matchs from './match';
import database from './database';

const rootReducer = combineReducers({
  authentification,
  matchs,
  database,
});
export default rootReducer;
