const authentification = (state = {}, action) => {
  switch (action.type) {
    case 'CREATE_USER_REQUEST':
      return {
        ...state,
        payload: action.payload,
        errorCreate: '',
      };
    case 'CREATE_USER_SUCCESS': {
      return {
        ...state,
        errorCreate: '',
        user: action.payload,
      }; }
    case 'CREATE_USER_ERROR':
      return {
        ...state,
        errorCreate: action.payload,
      };
    case 'LOGIN_USER_REQUEST':
      return {
        ...state,
        payload: action.payload,
        errorLogin: '',
      };
    case 'LOGIN_USER_SUCCESS': {
      return {
        ...state,
        errorLogin: '',
        user: action.payload,
      }; }
    case 'LOGIN_USER_ERROR':
      return {
        ...state,
        errorLogin: action.payload,
      };
    case 'DISCONNECT_USER_REQUEST':
      return {
        ...state,
      };
    case 'DISCONNECT_USER_SUCCESS':
      return {
        ...state,
        errorLogin: '',
        user: {},
      };
    case 'UPDATE_USER_REQUEST':
      return {
        ...state,
      };
    case 'UPDATE_USER_SUCCESS':
      return {
        ...state,
        user: action.payload,
      };
    case 'UPDATE_USER_FAILURE':
      return {
        ...state,
      };
    default:
      return state;
  }
};
export default authentification;
