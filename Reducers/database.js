const database = (state = {}, action) => {
  switch (action.type) {
    case 'TEAMS_REQUEST':
      return {
        ...state,
      };
    case 'TEAMS_SUCCESS':
      return {
        ...state,
        teams: action.payload,
      };
    case 'TEAMS_UPDATE':
      return {
        ...state,
      };
    case 'TEAMS_FAILURE':
      return {
        ...state,
      };
    case 'PICTURE_REQUEST':
      return {
        ...state,
      };
    case 'PICTURE_SUCCESS':
      return {
        ...state,
        picture: action.payload,
      };
    case 'PICTURE_UPDATE':
      return {
        ...state,
      };
    case 'PICTURE_FAILURE':
      return {
        ...state,
      };
    default:
      return state;
  }
};
export default database;
