import React from 'react';
import { Platform, View, YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import { createSwitchNavigator } from 'react-navigation';
import * as Expo from 'expo';
import Roboto from 'native-base/Fonts/Roboto.ttf';
import RobotoMedium from 'native-base/Fonts/Roboto_medium.ttf';
import '@babel/polyfill';

import './firebase/firebase';
import AuthScreen from './Components/Auth/AuthScreen';
import AuthLoadingScreen from './Components/Auth/AuthLoadingScreen';
import TabStack from './Components/TabStack';
import configureStore from './Store';
import rootReducer from './Reducers/rootReducer';

// Remove Warning on expo related to firebase token timer beeing to long for ReactNative
YellowBox.ignoreWarnings(['Setting a timer']);

const preloadedState = {
  authentification: {
    errorLogin: '',
    errorCreate: '',
    user: {},
  },
  database: {
    teams: [],
    picture: '',
  },
  matchs: {
    match: {},
  },
};

const store = configureStore(preloadedState);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./Reducers', () => {
    store.replaceReducer(rootReducer);
  });
}

const RootStack = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: TabStack,
    Auth: AuthScreen,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
    };
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto,
      Roboto_medium: RobotoMedium,
    });
    if (!(Platform.OS === 'android' && !Expo.Constants.isDevice)) {
      await Expo.Permissions.askAsync(Expo.Permissions.LOCATION);
    }
    this.setState({ fontLoaded: true });
  }

  render() {
    const { fontLoaded } = this.state;
    return (
      <Provider store={store}>
        {fontLoaded ? <RootStack /> : <View />}
      </Provider>
    );
  }
}
