import React from 'react';
import { View, Dimensions } from 'react-native';
import {
  List, ListItem, InputGroup, Text, Button, Input, Icon,
} from 'native-base';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class SignIn extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
    this.handleChangeText = this.handleChangeText.bind(this);
  }

  handleChangeText(field, value) {
    this.setState({ [field]: value });
  }

  render() {
    const { height: screenHeight } = Dimensions.get('window');
    const { width: screenWidth } = Dimensions.get('window');
    const { error, submit, changeComponent } = this.props;
    const { username, password } = this.state;

    return (
      <View style={{
        flex: 1, width: screenWidth - 20, height: screenHeight, justifyContent: 'center',
      }}
      >
        {error.toString() !== '' && (
        <Text>
          {error}
        </Text>
        )}
        <List>
          <ListItem>
            <InputGroup>
              <Icon name="ios-person" />
              <Input
                placeholder="Email"
                value={username}
                name="username"
                onChangeText={(text) => {
                  this.handleChangeText('username', text);
                }}
              />
            </InputGroup>
          </ListItem>
          <ListItem>
            <InputGroup>
              <Icon name="ios-unlock" />
              <Input
                placeholder="Password"
                secureTextEntry
                value={password}
                name="password"
                onChangeText={(text) => {
                  this.handleChangeText('password', text);
                }}
              />
            </InputGroup>
          </ListItem>
        </List>

        <Button
          full
          success
          iconRight
          onPress={() => {
            submit({ username, password });
          }}
          accessibilityLabel="Login to your SuperFootball account"
        >
          <Text>Login</Text>
          <Icon name="ios-arrow-forward" />
        </Button>


        <Button
          style={{ marginTop: 15 }}
          full
          success
          iconRight
          onPress={() => changeComponent()}
          accessibilityLabel="Sign Up to SuperFootball"
        >
          <Text>Sign Up</Text>
          <Icon name="ios-arrow-forward" />
        </Button>

      </View>
    );
  }
}

SignIn.propTypes = {
  error: PropTypes.string.isRequired,
  submit: PropTypes.func.isRequired,
  changeComponent: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error: state.authentification.errorLogin,
});

export default connect(mapStateToProps)(SignIn);
