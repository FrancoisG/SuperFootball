import React from 'react';
import { Dimensions, View } from 'react-native';
import {
  List, ListItem, InputGroup, Text, Button, Input, Icon,
} from 'native-base';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class SignUp extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      passwordConfirmation: '',
      firstname: '',
    };
    this.handleChangeText = this.handleChangeText.bind(this);
  }

  handleChangeText(field, value) {
    this.setState({ [field]: value });
  }

  render() {
    const { height: screenHeight } = Dimensions.get('window');
    const { error, submit, changeComponent } = this.props;
    const {
      username, password, passwordConfirmation, firstname,
    } = this.state;

    return (
      <View style={{ flex: 1, height: screenHeight, justifyContent: 'center' }}>
        {error.toString() !== '' && (
          <Text>
            {error}
          </Text>
        )}
        <List>
          <ListItem>
            <InputGroup>
              <Icon name="ios-person" />
              <Input
                placeholder="First Name"
                value={firstname}
                name="firstname"
                onChangeText={(text) => {
                  this.handleChangeText('firstname', text);
                }}
              />
            </InputGroup>
          </ListItem>
          <ListItem>
            <InputGroup>
              <Icon name="ios-person" />
              <Input
                placeholder="Email"
                value={username}
                name="username"
                onChangeText={(text) => {
                  this.handleChangeText('username', text);
                }}
              />
            </InputGroup>
          </ListItem>
          <ListItem>
            <InputGroup>
              <Icon name="ios-unlock" />
              <Input
                placeholder="Password"
                secureTextEntry
                value={password}
                name="password"
                onChangeText={(text) => {
                  this.handleChangeText('password', text);
                }}
              />
            </InputGroup>
          </ListItem>
          <ListItem>
            <InputGroup>
              <Icon name="ios-unlock" />
              <Input
                placeholder="Password Confirmation"
                secureTextEntry
                value={passwordConfirmation}
                name="passwordConfirmation"
                onChangeText={(text) => {
                  this.handleChangeText('passwordConfirmation', text);
                }}
              />
            </InputGroup>
          </ListItem>
        </List>
        <Button
          full
          success
          iconRight
          onPress={() => {
            submit({
              username,
              password,
              passwordConfirmation,
            });
          }}
          accessibilityLabel="Register to SuperFootball"
        >
          <Text>Register</Text>
          <Icon name="ios-arrow-forward" />
        </Button>
        <Text style={{ color: 'blue', marginTop: 15 }} onPress={() => changeComponent()}>
                    Already have an account? Click here to login.
        </Text>
      </View>
    );
  }
}

SignUp.propTypes = {
  error: PropTypes.string.isRequired,
  submit: PropTypes.func.isRequired,
  changeComponent: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error: state.authentification.errorCreate,
});

export default connect(mapStateToProps)(SignUp);
