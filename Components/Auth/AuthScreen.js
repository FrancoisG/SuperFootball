import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import Icon from '../../assets/icon.png';
import { createUserService, loginUserService } from '../../Actions/AuthAction';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class AuthScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      register: false,
    };
  }

  componentWillUpdate(nextProps) {
    const { user, navigation } = this.props;
    if (nextProps.user !== user) {
      navigation.navigate('App');
    }
  }

  changeComponent = () => {
    const { register } = this.state;
    this.setState({ register: !register });
  };

  render() {
    const { register } = this.state;
    const { loginUser, createUser } = this.props;
    return (
      <View style={styles.container}>
        <Image source={Icon} />
        {!register
          ? (
            <SignIn
              changeComponent={this.changeComponent}
              submit={creds => loginUser(creds)}
            />
          )
          : (
            <SignUp
              changeComponent={this.changeComponent}
              submit={creds => createUser(creds)}
            />
          )}
      </View>
    );
  }
}
AuthScreen.propTypes = {
  loginUser: PropTypes.func.isRequired,
  createUser: PropTypes.func.isRequired,
  user: PropTypes.shape().isRequired,
  navigation: PropTypes.shape().isRequired,
};


const mapStateToProps = state => ({
  user: state.authentification.user,
});
const mapDispatchToProps = dispatch => ({
  createUser: (cred) => {
    dispatch(createUserService(cred));
  },
  loginUser: (cred) => {
    dispatch(loginUserService(cred));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);
