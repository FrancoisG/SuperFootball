import React from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
import {
  ActivityIndicator, StatusBar, StyleSheet, View,
} from 'react-native';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';
import { isUserConnectedService } from '../../Actions/AuthAction';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class AuthLoadingScreen extends React.PureComponent {
  componentDidMount() {
    const { isUserConnected, navigation } = this.props;
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        isUserConnected();
        navigation.navigate('App');
      } else {
        navigation.navigate('Auth');
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

AuthLoadingScreen.propTypes = {
  isUserConnected: PropTypes.func.isRequired,
  navigation: PropTypes.shape().isRequired,
};

const mapDispatchToProps = dispatch => ({
  isUserConnected: () => dispatch(isUserConnectedService()),
});

export default connect(null, mapDispatchToProps)(AuthLoadingScreen);
