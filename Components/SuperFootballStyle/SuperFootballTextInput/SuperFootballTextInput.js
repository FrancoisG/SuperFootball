import React from 'react';
import PropTypes from 'prop-types';
import { Isao } from 'react-native-textinput-effects';

const SuperFootballTextInput = props => (
  <Isao
    {...props}
    activeColor="#33B2FF"
    passiveColor="#3E3E3E"
  />
);

SuperFootballTextInput.propTypes = {
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
};

export default SuperFootballTextInput;
