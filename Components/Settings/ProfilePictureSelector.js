import React from 'react';
import PropTypes from 'prop-types';
import {
  Image, StyleSheet, View,
} from 'react-native';
import * as Expo from 'expo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import noProfilePicture from '../../assets/profile.png';

const styles = StyleSheet.create({
  container: {
    margin: 5,
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    borderRadius: 50,
  },
  iconContainer: {
    backgroundColor: 'white',
    padding: 10,
    height: 40,
    width: 40,
    borderRadius: 20,
    textAlign: 'center',
    borderWidth: 1,
    borderColor: '#3E3E3E',
    margin: 8,
  },
  doubleIconContainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    paddingTop: 130,
  },
});

const pickImage = async (callback) => {
  await Expo.Permissions.askAsync(Expo.Permissions.CAMERA_ROLL);

  const result = await Expo.ImagePicker.launchImageLibraryAsync({
    allowsEditing: true,
    aspect: [1, 1],
    base64: true,
  });

  if (!result.cancelled) {
    callback(result.base64);
  }
};

const takeImage = async (callback) => {
  await Expo.Permissions.askAsync(Expo.Permissions.CAMERA);

  const result = await Expo.ImagePicker.launchCameraAsync({
    allowsEditing: true,
    aspect: [1, 1],
    base64: true,
  });

  if (!result.cancelled) {
    callback(result.base64);
  }
};

const ProfilePictureSelector = (props) => {
  const { base64Picture, updatePicture } = props;
  return (
    <View style={styles.container}>
      {base64Picture !== ''
        ? <Image source={{ uri: `data:image/jpg;base64,${base64Picture}` }} style={styles.image} />
        : <Image source={noProfilePicture} style={styles.image} /> }
      <View style={styles.doubleIconContainer}>
        <View style={styles.iconContainer}>
          <Ionicons onPress={() => pickImage(updatePicture)} name="md-images" size={20} color="#3E3E3E" />
        </View>
        <View style={styles.iconContainer}>
          <Ionicons onPress={() => takeImage(updatePicture)} name="md-camera" size={20} color="#3E3E3E" />
        </View>
      </View>
    </View>
  );
};

ProfilePictureSelector.propTypes = {
  base64Picture: PropTypes.string.isRequired,
  updatePicture: PropTypes.func.isRequired,
};

export default ProfilePictureSelector;
