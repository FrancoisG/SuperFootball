import React from 'react';
import { Button, StyleSheet, View } from 'react-native';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';
import { NavigationEvents, SafeAreaView } from 'react-navigation';

import {
  disconnectUserService,
  updateUserService,
} from '../../Actions/AuthAction';

import {
  updateProfilePictureService,
  getProfilePictureService,
} from '../../Actions/DatabaseActions';
import Header from '../Commons/Header';
import ProfilePictureSelector from './ProfilePictureSelector';
import UserVariablesEditor from './UserVariablesEditor';

const styles = StyleSheet.create({
  container: {
    margin: 5,
    flex: 1,
    backgroundColor: 'white',
  },
  button: {
    marginBottom: 10,
  },
});

class SettingsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      displayName: '',
      profilPic: '',
    };
  }

  componentDidMount() {
    const { user, getProfilePicture } = this.props;
    let tempDisplayName = user.displayName;
    if (tempDisplayName === null) tempDisplayName = '';
    this.setState({
      displayName: tempDisplayName,
    });
    getProfilePicture(user).then(() => {
      const { picture } = this.props;
      this.setState({ profilPic: picture });
    });
  }

  componentDidUpdate(prevProps) {
    const { user, navigation } = this.props;
    if (prevProps.user !== user) {
      if (user === null) {
        navigation.navigate('Auth');
      }
    }
  }

  onWillFocus = () => {

  };

  handleChangeText = (element, text) => {
    this.setState({ [element]: text });
  };

  save = () => {
    const {
      displayName, profilPic,
    } = this.state;
    const { updateUser, updateProfilePicture, user } = this.props;
    updateUser({ displayName });
    if (profilPic !== '') {
      updateProfilePicture(user, profilPic);
    }
  };

  disconnect = () => {
    const { disconnectUser } = this.props;
    disconnectUser();
  };

  updatePicture = (picture) => {
    this.setState({ profilPic: picture });
  };

  render() {
    const {
      displayName, profilPic,
    } = this.state;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
        <NavigationEvents
          onWillFocus={this.onWillFocus}
        />
        <Header title="Paramètres" />
        <View style={styles.container}>
          <ProfilePictureSelector base64Picture={profilPic} updatePicture={this.updatePicture} />
          <UserVariablesEditor
            displayName={displayName}
            handleChangeText={this.handleChangeText}
          />
          <View style={styles.button}>
            <Button title="Save" color="#33B2FF" onPress={this.save} />
          </View>
          <View style={styles.button}>
            <Button title="Disconnect" color="red" onPress={this.disconnect} />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

SettingsScreen.propTypes = {
  disconnectUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  navigation: PropTypes.shape().isRequired,
  user: PropTypes.shape().isRequired,
  getProfilePicture: PropTypes.func.isRequired,
  updateProfilePicture: PropTypes.func.isRequired,
  picture: PropTypes.string,
};

SettingsScreen.defaultProps = {
  picture: '',
};

const mapStateToProps = state => ({
  user: state.authentification.user,
  picture: state.database.picture,
});

const mapDispatchToProps = dispatch => ({
  disconnectUser: () => dispatch(disconnectUserService()),
  updateUser: (object) => { dispatch(updateUserService(object)); },
  updateProfilePicture: (user, object) => dispatch(updateProfilePictureService(user, object)),
  getProfilePicture: user => dispatch(getProfilePictureService(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
