import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import SuperFootballTextInput from '../SuperFootballStyle/SuperFootballTextInput/SuperFootballTextInput';


const styles = StyleSheet.create({
  container: {
    margin: 5,
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
  },
});

const UserVariablesEditor = (props) => {
  const {
    displayName, handleChangeText,
  } = props;
  return (
    <View style={styles.container}>
      <SuperFootballTextInput
        label="Nom d'utilisateur"
        value={displayName}
        onChangeText={(text) => {
          handleChangeText('displayName', text);
        }}
      />
    </View>
  );
};

UserVariablesEditor.propTypes = {
  displayName: PropTypes.string.isRequired,
  handleChangeText: PropTypes.func.isRequired,
};

export default UserVariablesEditor;
