const FootballTeams = [
  {
    id: 511,
    name: 'Toulouse FC',
    shortName: 'Toulouse',
    location: {
      latitude: 43.583282,
      longitude: 1.434123,
    },
  },
  {
    id: 514,
    name: 'SM Caen',
    shortName: 'Caen',
    location: {
      latitude: 49.179719,
      longitude: -0.396581,
    },
  },
  {
    id: 516,
    name: 'Olympique de Marseille',
    shortName: 'OM',
    location: {
      latitude: 43.269983,
      longitude: 5.396220,
    },
  },
  {
    id: 518,
    name: 'Montpellier HSC',
    shortName: 'Montpellier',
    location: {
      latitude: 43.622353,
      longitude: 3.812342,
    },
  },
  {
    id: 521,
    name: 'Lille OSC',
    shortName: 'LOSC',
    location: {
      latitude: 50.612076,
      longitude: 3.130930,
    },
  },
  {
    id: 522,
    name: "OGC de Nice Côte d'Azur",
    shortName: 'Nice',
    location: {
      latitude: 43.705152,
      longitude: 7.192907,
    },
  },
  {
    id: 523,
    name: 'Olympique Lyonnais',
    shortName: 'Lyon',
    location: {
      latitude: 45.765407,
      longitude: 4.982501,
    },
  },
  {
    id: 524,
    name: 'Paris Saint-Germain FC',
    shortName: 'PSG',
    location: {
      latitude: 48.841549,
      longitude: 2.253435,
    },
  },
  {
    id: 526,
    name: 'FC Girondins de Bordeaux',
    shortName: 'Bordeaux',
    location: {
      latitude: 44.897525,
      longitude: -0.561188,
    },
  },
  {
    id: 527,
    name: 'AS Saint-Étienne',
    shortName: 'St-Étienne',
    location: {
      latitude: 45.460987,
      longitude: 4.390535,
    },
  },
  {
    id: 528,
    name: "Dijon Football Côte d'Or",
    shortName: 'Dijon',
    location: {
      latitude: 47.324542,
      longitude: 5.069037,
    },
  },
  {
    id: 529,
    name: 'Stade Rennais FC 1901',
    shortName: 'Rennes',
    location: {
      latitude: 48.108014,
      longitude: -1.712634,
    },
  },
  {
    id: 530,
    name: 'Amiens SC',
    shortName: 'Amiens SC',
    location: {
      latitude: 49.893689,
      longitude: 2.263372,
    },
  },
  {
    id: 532,
    name: 'Angers SCO',
    shortName: 'Angers',
    location: {
      latitude: 47.460584,
      longitude: -0.530064,
    },
  },
  {
    id: 538,
    name: 'En Avant Guingamp',
    shortName: 'Guingamp',
    location: {
      latitude: 48.566335,
      longitude: -3.163848,
    },
  },
  {
    id: 543,
    name: 'FC Nantes',
    shortName: 'Nantes',
    location: {
      latitude: 47.256234,
      longitude: -1.524171,
    },
  },
  {
    id: 547,
    name: 'Stade de Reims',
    shortName: 'Stade de Reims',
    location: {
      latitude: 49.247253,
      longitude: 4.025481,
    },
  },
  {
    id: 548,
    name: 'AS Monaco FC',
    shortName: 'Monaco',
    location: {
      latitude: 43.727735,
      longitude: 7.415424,
    },
  },
  {
    id: 556,
    name: 'Nîmes Olympique',
    shortName: 'Nîmes',
    location: {
      latitude: 43.816203,
      longitude: 4.359695,
    },
  },
  {
    id: 576,
    name: 'RC Strasbourg Alsace',
    shortName: 'Strasbourg',
    location: {
      latitude: 48.560206,
      longitude: 7.755542,
    },
  },
];

export default FootballTeams;

// Ligue2: 2142
// Ligue1: 2015
// CL: 2001
