import React from 'react';
import {
  FlatList, StyleSheet, View,
} from 'react-native';
import { SafeAreaView, NavigationEvents } from 'react-navigation';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';

import FootballTeams from '../Constants';
import {
  updateSubscribedTeamsService,
} from '../../Actions/DatabaseActions';
import TeamDisplay from './TeamDisplay';
import Header from '../Commons/Header';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

class MyTeamsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formatedTeams: [],
    };
  }

  onWillFocus = () => {
    const formatedTeams = FootballTeams.map(team => ({
      ...team,
      loading: false,
      following: false,
    }));
    this.createStateFromProps(formatedTeams);
  };

  onPressTeam = (team) => {
    const { teams, user, updateSubscribedTeams } = this.props;
    let modifiedTeams = [];
    if (teams !== null && teams.length !== 0) {
      modifiedTeams = teams.filter(tempTeam => tempTeam.id !== team.id);
      if (modifiedTeams.length === teams.length) {
        modifiedTeams.push(team);
      }
    } else modifiedTeams.push(team);
    updateSubscribedTeams(user, modifiedTeams).then(() => {
      this.updateStateFromProps();
    });
  };

  updateStateFromProps = () => {
    const { teams } = this.props;
    const { formatedTeams } = this.state;
    const newTeams = formatedTeams.map((team) => {
      const filteredArray = teams.filter(teamFromProps => team.id === teamFromProps.id);
      if (filteredArray.length > 0) {
        return {
          ...team,
          following: true,
        };
      }
      return {
        ...team,
        following: false,
      };
    });
    this.setState({ formatedTeams: newTeams });
  };

  createStateFromProps = (formatedTeams) => {
    const { teams } = this.props;

    const newTeams = formatedTeams.map((team) => {
      const filteredArray = teams.filter(teamFromProps => team.id === teamFromProps.id);
      if (filteredArray.length > 0) {
        return {
          ...team,
          following: true,
        };
      }
      return {
        ...team,
        following: false,
      };
    });
    this.setState({ formatedTeams: newTeams });
  };

  render() {
    const { formatedTeams } = this.state;
    formatedTeams.sort((a, b) => a.name.localeCompare(b.name));
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
        <Header title="Mes équipes" />
        <View style={styles.container}>
          <NavigationEvents
            onWillFocus={this.onWillFocus}
          />
          <FlatList
            data={formatedTeams}
            renderItem={({ item }) => (
              <TeamDisplay team={item} onPressTeam={this.onPressTeam} />
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

MyTeamsScreen.propTypes = {
  user: PropTypes.shape().isRequired,
  updateSubscribedTeams: PropTypes.func.isRequired,
  teams: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  teams: state.database.teams,
  user: state.authentification.user,
});

const mapDispatchToProps = dispatch => ({
  updateSubscribedTeams: (user, teams) => dispatch(updateSubscribedTeamsService(user, teams)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyTeamsScreen);
