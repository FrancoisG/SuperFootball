import React from 'react';
import {
  StyleSheet, Text, View, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import logo from '../../assets/teamLogo';

const styles = StyleSheet.create({
  container: {
    margin: 5,
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#DDDDDD',
  },
  item: {
    paddingTop: 16,
    fontSize: 20,
    color: '#333',
    height: 68,
    flex: 1,
  },
  image: {
    width: 48, height: 48, margin: 10,
  },
  sub: {
    alignSelf: 'flex-end',
    height: 68,
    paddingTop: 8,
    paddingRight: 16,
  },
});

const TeamDisplay = (props) => {
  const { team, onPressTeam } = props;
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={logo[team.id]} />
      <Text
        onPress={() => onPressTeam(team)}
        style={styles.item}
      >
        {team.name}
      </Text>
      {team.following === true && (
        <Ionicons name="ios-checkmark-circle" size={44} color="#A7D874" style={styles.sub} />
      )}
    </View>
  );
};
TeamDisplay.propTypes = {
  team: PropTypes.shape().isRequired,
  onPressTeam: PropTypes.func.isRequired,
};


export default TeamDisplay;
