import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import * as Expo from 'expo';

const styles = StyleSheet.create({
  headerContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: 'black',
  },
  headerTitle: {
    fontSize: 26,
    height: 44,
    textAlign: 'center',
  },
  notch: {
    paddingTop: 50,
  },
});

const returnStyleDependingOnDevice = () => {
  if (Expo.Constants.deviceName === 'ONEPLUS A6003') {
    return [styles.headerContainer, styles.notch];
  }
  return [styles.headerContainer];
};

const Header = (props) => {
  const { title } = props;
  return (
    <View style={returnStyleDependingOnDevice()}>
      <Text style={styles.headerTitle}>{title}</Text>
    </View>
  );
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
