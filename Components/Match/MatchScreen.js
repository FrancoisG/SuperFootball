import React from 'react';
import { StyleSheet, ScrollView, RefreshControl } from 'react-native';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';
import { Location } from 'expo';

import { NavigationEvents, SafeAreaView } from 'react-navigation';
import MatchForTeam from './MatchForTeam';
import getMatchService from '../../Actions/MatchActions';
import { getSubscribedTeamsService } from '../../Actions/DatabaseActions';
import Header from '../Commons/Header';


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
  },
});

class MatchScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loadedMatch: false,
      teamChanged: true,
      refreshing: false,
      location: {
        latitude: null,
        longitude: null,
      },
    };
  }

  async componentDidMount() {
    const { user, getSubscribedTeams } = this.props;
    const location = await Location.getCurrentPositionAsync({});
    this.setState({
      location: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      },
    });
    getSubscribedTeams(user).then(() => {
      this.onWillFocus();
    });
  }

  componentDidUpdate(prevPros) {
    const { teams } = this.props;
    if (teams.length !== prevPros.teams.length) {
      this.onUpdate();
    }
  }

  onUpdate = () => {
    this.setState({ teamChanged: true });
  };

  onWillFocus = () => {
    const { getMatch, teams } = this.props;
    const { teamChanged } = this.state;
    if (teamChanged) {
      getMatch(teams).then(() => {
        this.setState({ loadedMatch: true, teamChanged: false });
      });
    }
  };

  onRefresh = () => {
    const { getMatch, teams } = this.props;
    this.setState({ refreshing: true });
    getMatch(teams).then(() => {
      this.setState({ loadedMatch: true, teamChanged: false, refreshing: false });
    });
  };

  render() {
    const { teams, match } = this.props;
    const { loadedMatch, refreshing, location } = this.state;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
        <Header title="Mes matchs" />
        <ScrollView
          style={styles.container}
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefresh}
            />
          )}
        >
          <NavigationEvents
            onWillFocus={this.onWillFocus}
          />
          {loadedMatch && teams.map(team => (
            typeof match[team.id] !== 'undefined'
          && (
          <MatchForTeam
            key={team.id}
            team={team}
            matchs={match[team.id]}
            location={location}
          />
          )
          ))}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

MatchScreen.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getMatch: PropTypes.func.isRequired,
  match: PropTypes.shape().isRequired,
  getSubscribedTeams: PropTypes.func.isRequired,
  user: PropTypes.shape().isRequired,
};

const mapStateToProps = state => ({
  teams: state.database.teams,
  match: state.matchs.match,
  user: state.authentification.user,
});

const mapDispatchToProps = dispatch => ({
  getSubscribedTeams: user => dispatch(getSubscribedTeamsService(user)),
  getMatch: teamId => dispatch(getMatchService(teamId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MatchScreen);
