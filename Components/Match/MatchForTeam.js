import React from 'react';
import {
  View, Text, StyleSheet, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import geolib from 'geolib';
import 'moment/locale/fr';
import FootballTeams from '../Constants';
import logo from '../../assets/teamLogo';

moment.locale('fr');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
  },
  containerColumn: {
    flexDirection: 'column',
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
  },
  containerRow: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#DDDDDD',
  },
  matchContainer: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    padding: 15,
    borderBottomWidth: 1,
    borderColor: '#DDDDDD',
  },
  team: {
    paddingTop: 6,
    fontSize: 20,
    color: '#3E3E3E',
    height: 48,
    flex: 1,
    textAlign: 'center',
  },
  teamName: {
    color: '#3E3E3E',
    paddingTop: 16,
    fontSize: 20,
    height: 68,
    fontWeight: 'bold',
    flex: 1,
  },
  image: {
    width: 48, height: 48, margin: 10,
  },
  teamLogo: {
    width: 28, height: 28, marginLeft: 10, marginTop: 4,
  },
  dayText: {
    flex: 1,
    height: 25,
    paddingTop: 5,
    textAlign: 'center',
  },
  timeText: {
    flex: 1,
    height: 25,
    paddingTop: 5,
    textAlign: 'center',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    flexDirection: 'column',
  },
  matchTimeContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
  },
  teamsContainer: {
    flex: 2,
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
    borderRightWidth: 1,
    borderColor: '#DDDDDD',
  },
  teamNameSmall: {
    height: 30,
    marginLeft: 10,
    paddingTop: 10,
  },
  winner: {
    backgroundColor: '#e6ffe6',
  },
  tied: {
    backgroundColor: '#ffffe6',
  },
  looser: {
    backgroundColor: '#ffe6e6',
  },
  done: {
    fontWeight: 'bold',
  },
});

const MatchForTeam = (props) => {
  const getTeamById = (teamId) => {
    const teams = FootballTeams.filter(team => team.id === teamId);
    if (teams.length > 0) return teams[0];
    return {
      shortName: 'unknown',
    };
  };

  const getDistance = (team) => {
    const { location } = props;
    return parseInt(geolib.getDistanceSimple(location, team.location) / 1000, 10);
  };


  const { team, matchs } = props;

  return (
    <View style={styles.containerColumn}>
      <View style={styles.containerRow}>
        <Image style={styles.image} source={logo[team.id]} />
        <Text style={styles.teamName}>
          {team.name}
        </Text>
      </View>
      {typeof matchs !== 'undefined' && matchs.map((match) => {
        const homeTeam = getTeamById(match.homeTeam);
        const awayTeam = getTeamById(match.awayTeam);
        const distance = getDistance(homeTeam);
        const date = moment(match.date);
        const day = date.format('ddd  DD/MM');
        const hour = date.format('LT');
        const styleHome = [styles.row];
        const styleAway = [styles.row];
        if (match.winner) {
          if (match.homeTeam === team.id) {
            if (match.winner === 'HOME_TEAM') {
              styleHome.push(styles.winner);
            } else if (match.winner === 'AWAY_TEAM') {
              styleHome.push(styles.looser);
            } else {
              styleHome.push(styles.tied);
            }
          } else if (match.winner === 'AWAY_TEAM') {
            styleAway.push(styles.winner);
          } else if (match.winner === 'HOME_TEAM') {
            styleAway.push(styles.looser);
          } else {
            styleAway.push(styles.tied);
          }
        }
        return (
          <View style={styles.matchContainer} key={match.date}>
            <View style={styles.teamsContainer}>
              <View style={styleHome}>
                <Image style={styles.teamLogo} source={logo[match.homeTeam]} />
                {match.scoreHome !== null ? <Text style={styles.teamNameSmall}>{`${match.scoreHome}    - `}</Text> : null}
                <Text style={styles.teamNameSmall}>
                  {homeTeam.shortName}
                </Text>

              </View>
              <View style={styleAway}>
                <Image style={styles.teamLogo} source={logo[match.awayTeam]} />
                {match.scoreAway !== null ? <Text style={styles.teamNameSmall}>{`${match.scoreAway}    - `}</Text> : null}
                <Text style={styles.teamNameSmall}>
                  {awayTeam.shortName}
                </Text>

              </View>
            </View>
            <View style={styles.matchTimeContainer}>
              {match.status === 'FINISHED' ? <Text style={[styles.timeText, styles.done]}>Terminé</Text>
                : (
                  <Text style={styles.timeText}>
                    {hour}
                  </Text>
                )}
              <Text style={styles.dayText}>
                {day}
              </Text>
              <Text style={styles.dayText}>
                {`${distance} km`}
              </Text>
            </View>
          </View>
        );
      })}
    </View>
  );
};

MatchForTeam.propTypes = {
  team: PropTypes.shape().isRequired,
  location: PropTypes.shape().isRequired,
  matchs: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default MatchForTeam;
