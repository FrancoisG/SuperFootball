import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import SettingsScreen from './Settings/SettingsScreen';
import MyTeamsScreen from './MyTeams/MyTeamsScreen';
import MatchScreen from './Match/MatchScreen';

const TabStack = createBottomTabNavigator(
  {
    Match: MatchScreen,
    MyTeams: MyTeamsScreen,
    Settings: SettingsScreen,
  },
  {
    navigationOptions: ({ navigation }) => ({
      // eslint-disable-next-line
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Match') {
          iconName = 'ios-football';
        } else if (routeName === 'MyTeams') {
          iconName = 'ios-shirt';
        } else if (routeName === 'Settings') {
          iconName = 'ios-options';
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  },
);

export default TabStack;
